<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  protected $fillable = [
      'name', 'email', 'cpf','tel','cel',
  ];
  public $rules = [
        'name' => 'required|min:3|max:100',
        'email' => 'required|email|',
        'cpf' => 'required|min:11|unique:clients',

      ];

  public $messages =[
          'name.required' => 'O campo nome é obrigatório!',
          'name.min' => 'O campo nome é curto demais!',
          'email.required' => 'O campo email é obrigatório!',
          'cpf.required' => 'O campo cpf é obrigatório!',
          'cpf.min' => 'O campo cpf não tem o minimo necessário!',
          'cpf.unique' => 'O campo cpf já está em uso!'

        ];
        public $rulesupdate = [
              'name' => 'required|min:3|max:100',
              'email' => 'required|email|',
              'cpf' => 'required|min:11',

            ];

}
