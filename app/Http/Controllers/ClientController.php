<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;


class ClientController extends Controller
{
  private $client;

  public function __construct(Client $client){
    $this->client = $client;
  }

  public function list(){
    $clients = $this->client->all();

    return view('clients', compact('clients'));
      }

  public function delete($id)
  {
    $client = $this->client->findorFail($id);
    $client->delete();

    return redirect()->back();

  }

  public function update($id,Request $request)
  {
    $client = $this->client->find($id);
    $dataForm = $request->all();

    $valores = array('(',')','.','-', ' ');
    $Data =[
      'name' => $dataForm['name'],
      'email' => $dataForm['email'],
      'cpf' => str_replace($valores,'' ,$dataForm['cpf']),
      'tel' => str_replace($valores,'',$dataForm['tel']),
      'cel' => str_replace($valores,'', $dataForm['cel'])
    ];
    $cpf = $Data['cpf'];
    if (strlen($cpf) != 11){
        return redirect()->back();
    }else{
      if(($cpf{0}==$cpf{1})&&($cpf{1}==$cpf{2})&&($cpf{2}==$cpf{3})&&
          ($cpf{3}==$cpf{4})&&($cpf{4}==$cpf{5})&&($cpf{5}==$cpf{6})&&
          ($cpf{6}==$cpf{7})&&($cpf{7}==$cpf{8})&&($cpf{8}==$cpf{9})&&
          ($cpf{9}==$cpf{10})){
            return redirect()->back();
          }else {
            for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--){
              $soma += $cpf{$i} * $j;
            }
            $resto = ($soma*10) % 11;

            if($resto == 10 || $resto == 11){
              $resto = 0;
            }
            if($resto != $cpf{9}){
              return redirect()->back();
            }else{
              for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--){
                $soma += $cpf{$i} * $j;
              }
              $resto = ($soma*10) % 11;

              if($resto == $cpf{10}){
                $validate = validator($Data, $this->client->rulesupdate, $this->client->messages);
                if( $validate->fails()){
                  return redirect()
                          ->back()
                          ->withErrors($validate,'editclient')
                          ->withInput();
                }

                $client->update($Data);

                return redirect()->back();
              }else{
                return redirect()->back();
              }
            }
        }
    }
  }

  public function register(Request $request)
  {

      $dataForm = $request->all();
      $valores = array('(',')','.','-', ' ');
      $Data =[
        'name' => $dataForm['name'],
        'email' => $dataForm['email'],
        'cpf' => str_replace($valores,'' ,$dataForm['cpf']),
        'tel' => str_replace($valores,'',$dataForm['tel']),
        'cel' => str_replace($valores,'', $dataForm['cel'])
      ];
      $cpf = $Data['cpf'];
      if (strlen($cpf) != 11){
          return redirect()->back();
      }else{
        if(($cpf{0}==$cpf{1})&&($cpf{1}==$cpf{2})&&($cpf{2}==$cpf{3})&&
            ($cpf{3}==$cpf{4})&&($cpf{4}==$cpf{5})&&($cpf{5}==$cpf{6})&&
            ($cpf{6}==$cpf{7})&&($cpf{7}==$cpf{8})&&($cpf{8}==$cpf{9})&&
            ($cpf{9}==$cpf{10})){
              return redirect()->back();
            }else {
            	for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--){
                $soma += $cpf{$i} * $j;
              }
            	$resto = ($soma*10) % 11;

              if($resto == 10 || $resto == 11){
                $resto = 0;
              }
              if($resto != $cpf{9}){
                return redirect()->back();
              }else{
                for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--){
                  $soma += $cpf{$i} * $j;
                }
              	$resto = ($soma*10) % 11;

                if($resto == $cpf{10}){
                  $validate = validator($Data, $this->client->rules, $this->client->messages);
                  if( $validate->fails()){
                    return redirect()
                            ->back()
                            ->withErrors($validate,'registerClient')
                            ->withInput();
                  }

                  $this->client->create($Data);

                  return redirect()->back();
                }else{
                  return redirect()->back();
                }
              }
          }
      }
  }
}
