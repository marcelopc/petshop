<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Client;
use App\role;
use Illuminate\Http\Request;

class UserController extends Controller
{
  private $user;
  private $client;

    public function __construct(User $user,Client $client)
    {
        $this->user = $user;
        $this->client = $client;
    }

    public function list(){

      $users = $this->user->select('users.id','name','email','created_at','role')
                          ->join('roles', 'users.role_id','=','roles.id')->get();

      return view('listuser', compact('users'));
    }

    public function delete($id)
    {
      $this->authorize('admin', Auth::user()->role_id);
      $user = $this->user->findorFail($id);
      $user->delete();

      return redirect()->back();

    }

    public function update($id,Request $request)
    {
      $this->authorize('admin', Auth::user()->role_id);
      $user = $this->user->find($id);

      $dataForm = $request->all();

      $validate = validator($dataForm, $this->user->rulesupdate, $this->user->messages);
      if( $validate->fails()){
        return redirect()
                ->back()
                ->withErrors($validate,'edit')
                ->withInput();
      }
      if ($dataForm['password'] == null) {
        $data = [
          '_token' => $dataForm['_token'],
          'name' => $dataForm['name'],
          'email' => $dataForm['email'],
          'role_id' => $dataForm['role_id']
        ];
      }else{
        $data = [
          '_token' => $dataForm['_token'],
          'name' => $dataForm['name'],
          'email' => $dataForm['email'],
          'role_id' => $dataForm['role_id'],
          'password' => bcrypt($dataForm['password'])
        ];
      }
      $user->update($data);

      return redirect()->back();

    }


    public function index()
    {

        $users = $this->user->select('users.id','name','email','created_at','role')
                            ->join('roles', 'users.role_id','=','roles.id')
                            ->limit(5)->latest()->get();
        $clients = $this->client->limit(5)->latest()->get();

        return view('/dashboard',compact('users','clients'));
    }


    public function register(Request $request)
    {
      $this->authorize('admin', Auth::user()->role_id);

        $dataForm = $request->all();

        $validate = validator($dataForm, $this->user->rules, $this->user->messages);
        if( $validate->fails()){
          return redirect()
                  ->back()
                  ->withErrors($validate,'register')
                  ->withInput();
        }

        $data = [
          '_token' => $dataForm['_token'],
          'name' => $dataForm['name'],
          'email' => $dataForm['email'],
          'role_id' => $dataForm['role_id'],
          'password' => bcrypt($dataForm['password'])
        ];

        $this->user->create($data);

        return redirect()->back();

    }
}
