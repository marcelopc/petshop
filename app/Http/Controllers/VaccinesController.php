<?php

namespace App\Http\Controllers;

use App\Vaccine;
use Illuminate\Http\Request;

class VaccinesController extends Controller
{
    private $vaccine;

    public function __construct(Vaccine $vaccine)
    {
        $this->vaccine = $vaccine;
    }

    public function list()
    {
        $vaccines = $this->vaccine->all();

        return view('vaccines', compact('vaccines'));
    }
    public function delete($id)
    {
      $vaccine = $this->vaccine->findorFail($id);
      $vaccine->delete();

      return redirect()->back();
    }
    public function update($id,Request $request)
    {
      $vaccine = $this->vaccine->find($id);
      $dataForm = $request->all();

    /*
        $validate = validator($Data, $this->client->rulesupdate, $this->client->messages);
        if( $validate->fails()){
          return redirect()
                  ->back()
                  ->withErrors($validate,'editclient')
                  ->withInput();
        }*/

        $vaccine->update($dataForm);

        return redirect()->back();
    }

    public function register(Request $request)
    {
        $request = $request->all();

        $this->vaccine->create($request);

        return redirect()->back();
    }
}
