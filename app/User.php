<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','password_confirm','role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $rules = [
          'name' => 'required|min:3|max:100',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6',
          'password_confirm' => 'required|min:6|same:password',
          'role_id' => 'required',
        ];

    public $messages =[
            'name.required' => 'O campo nome é obrigatorio!',
            'name.min' => 'O campo nome é curto demais!',
            'email.required' => 'O campo email é obrigatorio!',
            'password.required' => 'O campo senha é obrigatorio!',
            'password.confirmed' => 'A confirmação de senha não confere!',
            'password.min' => 'Senha deve conter mais de 6 caracteres!',
            'password_confirm.same'=>'Confirmação de senha e senha devem ser iguais.',
            'role_id.required' => 'O campo função é obrigatorio!',
          ];

          public $rulesupdate = [
                'name' => 'required|min:3|max:100',
                'email' => 'required|email|',
                'role_id' => 'required',
              ];


}
