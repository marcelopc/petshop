$(function(){
  $('.date').datepicker({
      format: 'dd/mm/yyyy',
      startDate: '+0d',
      language: "pt-BR"
  });
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

function cpfvalidator(){
  var cpf = document.getElementById('InputCpf').value;
  var cpf2 = document.getElementById('editCpfClient').value;

  if(cpf2 == ""){
    var array = cpf.split("");
  }else{
    var array = cpf2.split("");
  }

  if ((array[0]==array[1])&&(array[1]==array[2])&&(array[2]==array[4])&&
(array[4]==array[5])&&(array[5]==array[6])&&(array[6]==array[8])&&
(array[8]==array[9])&&(array[9]==array[10])&&(array[10]==array[12])&&
(array[12]==array[13])) {
  if(cpf2 == ""){
    $('#InputCpf').css("borderColor","#e96666","display","block");
    $('.alertcad').css("display","block");
    $('#send').attr('disabled','disabled');
  }else{
    $('#editCpfClient').css("borderColor","#e96666","display","block");
    $('.alertedit').css("display","block");
    $('#send').attr('disabled','disabled');
  }
  }else {
    var sum = (array[0]*10)+(array[1]*9)+(array[2]*8)+(array[4]*7)+(array[5]*6)+
                (array[6]*5)+(array[8]*4)+(array[9]*3)+(array[10]*2);
    result = (sum*10)%11;

    if(result == 10 || result == 11){
      result = 0;
    }else if(result == array[12]){
        sum = 0;
        result =0;

        sum = (array[0]*11)+(array[1]*10)+(array[2]*9)+(array[4]*8)+(array[5]*7)+
                    (array[6]*6)+(array[8]*5)+(array[9]*4)+(array[10]*3)+(array[12]*2);
          result = (sum*10)%11;
          if(result == array[13]){
            if(cpf2 == ""){
              $('#InputCpf').css("borderColor","#ccc");
              $('.alert').css("display","none");
              $('#send').removeAttr('disabled');
            }else{
              $('#editCpfClient').css("borderColor","#ccc");
              $('.alertedit').css("display","none");
              $('#send').removeAttr('disabled');
            }
          }else{
            if(cpf2 == ""){
              $('#InputCpf').css("borderColor","#e96666","display","block");
              $('.alertcad').css("display","block");
              $('#send').attr('disabled','disabled');
            }else{
              $('#editCpfClient').css("borderColor","#e96666","display","block");
              $('.alertedit').css("display","block");
              $('#send').attr('disabled','disabled');
            }
          }
    }else{
      if(cpf2 == ""){
        $('#InputCpf').css("borderColor","#e96666","display","block");
        $('.alertcad').css("display","block");
        $('#send').attr('disabled','disabled');
      }else{
        $('#editCpfClient').css("borderColor","#e96666","display","block");
        $('.alertedit').css("display","block");
        $('#send').attr('disabled','disabled');
      }
    }
  }

};


$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

$(document).ready(function(){
  $('.tdcel').mask('(00) 00000-0000');
  $('.tdtel').mask('(00) 0000-0000');
  $('.tdcpf').mask('000.000.000-00', {reverse: true});

});


$( document ).ready( function() {
  $(".responsive-calendar").responsiveCalendar();
});


$(document).ready(function()
{
  $(".dataTable").dataTable({
            "bFilter": true,
            "bInfo": false,
            "pageLength": 10,
            "bSort": true,
            "columnDefs": [ {
                            "targets": 4,
                            "orderable": false
                          } ],
            "language": {"sEmptyTable": "Nenhum registro encontrado",
                          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                          "sInfoPostFix": "",
                          "sInfoThousands": ".",
                          "sLengthMenu": "_MENU_ resultados por página",
                          "sLoadingRecords": "Carregando...",
                          "sProcessing": "Processando...",
                          "sZeroRecords": "Nenhum registro encontrado",
                          "sSearch": "Pesquisar",
                          "oPaginate": {
                              "sNext": "Próximo",
                              "sPrevious": "Anterior",
                              "sFirst": "Primeiro",
                              "sLast": "Último"
                          }
                        }


    });
});

$(document).ready(function()
{
  $(".dataTableClient").dataTable({
            "bFilter": true,
            "bInfo": false,
            "pageLength": 10,
            "bSort": true,
            "columnDefs": [ {
                            "targets": 6,
                            "orderable": false
                          } ],
            "language": {"sEmptyTable": "Nenhum registro encontrado",
                          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                          "sInfoPostFix": "",
                          "sInfoThousands": ".",
                          "sLengthMenu": "_MENU_ resultados por página",
                          "sLoadingRecords": "Carregando...",
                          "sProcessing": "Processando...",
                          "sZeroRecords": "Nenhum registro encontrado",
                          "sSearch": "Pesquisar",
                          "oPaginate": {
                              "sNext": "Próximo",
                              "sPrevious": "Anterior",
                              "sFirst": "Primeiro",
                              "sLast": "Último"
                          }
                        }


    });
});

$(document).ready(function()
{
  $(".dataTableVaccine").dataTable({
            "bFilter": true,
            "bInfo": false,
            "pageLength": 10,
            "bSort": true,
            "columnDefs": [ {
                            "targets": 2,
                            "orderable": false
                          } ],
            "language": {"sEmptyTable": "Nenhum registro encontrado",
                          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                          "sInfoPostFix": "",
                          "sInfoThousands": ".",
                          "sLengthMenu": "_MENU_ resultados por página",
                          "sLoadingRecords": "Carregando...",
                          "sProcessing": "Processando...",
                          "sZeroRecords": "Nenhum registro encontrado",
                          "sSearch": "Pesquisar",
                          "oPaginate": {
                              "sNext": "Próximo",
                              "sPrevious": "Anterior",
                              "sFirst": "Primeiro",
                              "sLast": "Último"
                          }
                        }


    });
});
