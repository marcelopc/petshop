@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

            <div class="panel panel-default panel-login">

                <div class="panel-body">
                    <form class="form-horizontal"
                    method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group
                        {{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-6">
                                <input id="email" type="email"
                                class="form-control input-email" name="email"
                                value="{{ old('email') }}"
                                placeholder="E-mail" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group
                        {{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-6">
                                <input id="password" type="password"
                                class="form-control input-pw" name="password"
                                placeholder="Password" required >

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary btn-login">
                                    Login
                                </button>

                                <a class="btn btn-link"
                                href="{{ route('password.request') }}">
                                    Esqueceu sua senha?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

    </div>
</div>

@endsection
