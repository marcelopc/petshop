@extends('layouts.layout')

@section('content')


  <div class="col-lg-12">
    <div class="contentlistclients">
      <div class="btn-right">
        <button type="button" name="button" class="btn-action"data-toggle='modal'
        data-target='#cadclient'><i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar</button>
      </div>

      <h3 class="text-center">Clientes</h3>


          <table  class="table table-striped table-bordered table-hover dataTableClient no-footer" cellspacing="0">
            <thead class="thead-inverse">
              <tr>
                <th class="text-center">Nome</th>
                <th class="text-center">CPF</th>
                <th class="text-center">Tel</th>
                <th class="text-center">Cel</th>
                <th class="text-center">email</th>
                <th class="text-center">cadastro</th>
                <th class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($clients as $client)
                <tr id='{{$client->id}}'>
                  <td class="text-center">{{$client->name}}</td>
                  <td class="text-center tdcpf">{{$client->cpf}}</td>
                  <td class="text-center tdtel">{{$client->tel}}</td>
                  <td class="text-center tdcel">{{$client->cel}}</td>
                  <td class="text-center">{{$client->email}}</td>
                  <td class="text-center">{{date_format($client->created_at, 'd/m/Y')}}</td>
                    <td class="text-center">
                      <div>

                          <button onclick="checkCols('{{$client->id}}','editclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#editmodalclient'>
                          <i class="fa fa-pencil" data-toggle='tooltip'data-placement="top"
                          title="Editar" aria-hidden="true"></i></button>

                          <button onclick="checkCols('{{$client->id}}','deleteclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#deletemodalclient'>
                          <i class="fa fa-trash" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Excluir"></i></button>

                          <button onclick="checkCols('{{$client->id}}','deleteclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#petmodalclient'>
                          <i class="fa fa-paw" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Pet"></i></button>

                          <button onclick="checkCols('{{$client->id}}','pet')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#detalhesmodalclient'>
                          <i class="fa fa-eye" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Detalhes"></i></button>
                      </div>
                      </td>
                </tr>

              @endforeach
            </tbody>

          </table>


    </div>
  </div>


<script>
function checkCols (id,tipo)
{

  var linha = document.getElementById(id);
  var colunas = linha.getElementsByTagName('td');

if(tipo == 'deleteclient'){
  var div = document.getElementById('divdeleteclient');
  div.innerHTML = "<p class='pdelete text-center'> Deseja deletar o(a) cliente "+colunas[0].innerHTML+"</p>";
  document.formdelclient.action = '{{url('/deleteclient')}}'+'/'+id;
}else if(tipo == 'editclient'){
  document.getElementById('editNomeClient').value = colunas[0].innerHTML;
  document.getElementById('editCpfClient').value = colunas[1].innerHTML;
  document.getElementById('editTelClient').value = colunas[2].innerHTML;
  document.getElementById('editCelClient').value = colunas[3].innerHTML;
  document.getElementById('editEmailClient').value = colunas[4].innerHTML;


  document.formeditclient.action = '{{url('/editclient')}}'+'/'+id;
  }
}
</script>

<!--Janela modal Cadastrar Clientes-->
<div class="modal fade" id="cadclient" role="dialog" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times</span>
          <span class="sr-only">Cadastrar Funcionário</span>
        </button>
        <h4 class="modal-title text-center">Cadastrar Cliente</h4>
      </div>
      <div class="modal-body">

        @if( $errors->registerClient->any())
          <div class="alert alert-danger">
            @foreach ($errors->registerClient->all() as $error)
              <p class="pdelete">{{$error}}</p>
            @endforeach
          </div>
        @endif
        {!!Form::open(['url'=>'clientreg'])!!}

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="InputNome">Nome</label>
                  <input name="name" type="text"
                  class="form-control" id="InputNome"
                  placeholder="Nome" focus>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="InputEmail1">Endereço de e-mail</label>
                  <input name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                </div>
              </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputCpf">C.P.F</label>
                    <input name="cpf" type="text" class="form-control tdcpf" id="InputCpf" placeholder="CPF" onblur="cpfvalidator()">

                    <div class="alert alert-danger alert-dismissible alertcad" style="display: none">
                      <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times</span>
                      </button>
                      CPF inválido!
                    </div>

                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="InputTel">Telefone</label>
                  <input name="tel" type="text" class="form-control tdtel" id="InputTel" placeholder="Telefone">
                </div>
              </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputCel">Celular</label>
                    <input name="cel" type="text" class="form-control tdcel" id="InputCel" placeholder="Celular">
                  </div>
              </div>
            </div>

          <div class="modal-footer">
            <button id="send" type="submit" class="btn btn-success">Enviar</button>
            <button class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
          {!!Form::close()!!}


      </div>

    </div>
  </div>
</div>
<!--Janela modal Cadastrar cliente-->

<!--Janela modal Editar cliente-->
<div class="modal fade" id="editmodalclient" role="dialog" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times</span>
          <span class="sr-only">Editar Cliente</span>
        </button>
        <h4 class="modal-title text-center">Editar Cliente</h4>
      </div>
      <div class="modal-body">

        @if( $errors->editclient->any())
          <div class="alert alert-danger pdelete">
            @foreach ($errors->editclient->all() as $error)
              <p class="pdelete">{{$error}}</p>
            @endforeach
          </div>
        @endif
        {!!Form::open(['method' => 'PATCH','name' =>'formeditclient'])!!}

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label for="editNomeClient">Nome</label>
              <input name="name" type="text"
              class="form-control" id="editNomeClient"
              placeholder="Nome" focus>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label for="editEmailClient">Endereço de e-mail</label>
              <input name="email" type="email" class="form-control" id="editEmailClient" placeholder="Email">
            </div>
          </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="editCpfClient">C.P.F</label>
                <input name="cpf" type="text" class="form-control tdcpf" id="editCpfClient" placeholder="CPF"  onblur="cpfvalidator()">

                <div class="alert alert-danger alert-dismissible alertedit" style="display: none">
                  <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times</span>
                  </button>
                  CPF inválido!
                </div>
              </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label for="editTelClient">Telefone</label>
              <input name="tel" type="text" class="form-control tdtel" id="editTelClient" placeholder="Telefone">
            </div>
          </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="editCelClient">Celular</label>
                <input name="cel" type="text" class="form-control tdcel" id="editCelClient" placeholder="Celular">
              </div>
          </div>
        </div>

      <div class="modal-footer">
        <button id="send" type="submit" class="btn btn-success">Enviar</button>
        <button class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
          {!!Form::close()!!}


      </div>

    </div>
  </div>
</div>
<!--Janela modal editar Cliente-->

<!-- Janela modal Delete Cliente -->
<div class="modal fade" tabindex="-1" role="dialog" id="deletemodalclient">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body" id="divdeleteclient">
      </div>
      {!!Form::open(['method' => 'DELETE','name' =>'formdelclient'])!!}
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Deletar</button>
      </div>
      {!!Form::close()!!}
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Janela modal Delete Cliente -->



<!--Janela modal Cadastrar Clientes-->
<div class="modal fade" id="petmodalclient" role="dialog" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times</span>
          <span class="sr-only">Cadastrar Funcionário</span>
        </button>
        <h4 class="modal-title text-center">Cadastrar Cliente</h4>
      </div>
      <div class="modal-body">

        @if( $errors->registerClient->any())
          <div class="alert alert-danger">
            @foreach ($errors->registerClient->all() as $error)
              <p class="pdelete">{{$error}}</p>
            @endforeach
          </div>
        @endif
        {!!Form::open(['url'=>'clientreg'])!!}

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="InputNome">Nome</label>
                  <input name="name" type="text"
                  class="form-control" id="InputNome"
                  placeholder="Nome" focus>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="InputEmail1">Endereço de e-mail</label>
                  <input name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                </div>
              </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputCpf">C.P.F</label>
                    <input name="cpf" type="text" class="form-control tdcpf" id="InputCpf" placeholder="CPF" onblur="cpfvalidator()">

                    <div class="alert alert-danger alert-dismissible alertcad" style="display: none">
                      <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times</span>
                      </button>
                      CPF inválido!
                    </div>

                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="InputTel">Telefone</label>
                  <input name="tel" type="text" class="form-control tdtel" id="InputTel" placeholder="Telefone">
                </div>
              </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputCel">Celular</label>
                    <input name="cel" type="text" class="form-control tdcel" id="InputCel" placeholder="Celular">
                  </div>
              </div>
            </div>

          <div class="modal-footer">
            <button id="send" type="submit" class="btn btn-success">Enviar</button>
            <button class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
          {!!Form::close()!!}


      </div>

    </div>
  </div>
</div>
<!--Janela modal Cadastrar cliente-->





@endsection
