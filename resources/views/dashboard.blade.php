@extends('layouts.layout')

@section('content')
  @if (Auth::user()->role_id == 1)
    <!-- Employer-->
      <div class="col-lg-6 employeer">
        <div class="contentemployeer">

          <div class="btn-right">
            <button type="button" name="button" class="btn-action"data-toggle='modal'
            data-target='#cadfun'><i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar</button>
            <a href={{url('/listuser')}} class="btn-action"><i class="fa fa-eye" aria-hidden="true"></i> Ver todos</a>
          </div>

          <h3 class="text-left">Funcionários</h3>

            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="thead-inverse">
                  <tr>
                    <th class="text-center">Nome</th>
                    <th class="text-center">E-mail</th>
                    <th class="text-center">Função</th>
                    <th class="text-center">Cadastro</th>
                    <th class="text-center">Ações</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($users as $user)

                    <tr id='1-{{$user->id}}'>
                        <td class="text-center">{{$user->name}}</td>
                        <td class="text-center">{{$user->email}}</td>
                        <td class="text-center">{{$user->role}}</td>
                        <td class="text-center">{{date_format($user->created_at, 'd/m/Y')}}</td>
                        <td class="text-center">
                          <div class="">

                              <button onclick="checkCols('1-{{$user->id}}','edit')"
                              type="button" name="button" class="btn-action"
                              data-toggle='modal' data-target='#editmodal'>
                              <i class="fa fa-pencil" data-toggle='tooltip'data-placement="top"
                              title="Editar" aria-hidden="true"></i></button>
                              <button onclick="checkCols('1-{{$user->id}}','delete')" type="button" name="button" class="btn-action"
                              data-toggle='modal' data-target='#deletemodal'>
                              <i class="fa fa-trash" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                              title="Excluir"></i></button>
                          </div>
                          </td>
                    </tr>

                  @endforeach
                </tbody>
              </table>
            </div>

        </div>
      </div>
      <!-- ./Employer-->
  @endif


<div class="col-lg-6">
  <div class="tam ">
    <!-- Responsive calendar - START -->
    <div class="responsive-calendar">
      <div class="controls">
          <a class="pull-left" data-go="prev"><div class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i></div></a>
          <h4><span data-head-month></span> <span data-head-year></span> </h4>
          <a class="pull-right" data-go="next"><div class="btn btn-primary"><i class="fa fa-arrow-right" aria-hidden="true"></i></div></a>
      </div><hr/>
      <div class="day-headers">
        <div class="day header">Seg</div>
        <div class="day header">Ter</div>
        <div class="day header">Qua</div>
        <div class="day header">Qui</div>
        <div class="day header">Sex</div>
        <div class="day header">Sab</div>
        <div class="day header">Dom</div>
      </div>
      <div class="days" data-group="days">
        <!-- the place where days will be generated -->
      </div>
    </div>
    <!-- Responsive calendar - END -->
  </div>

</div>


<!-- Clients-->
  <div class="col-md-12 col-lg-7">
    <div class="contentClients">

      <div class="btn-right">
        <button type="button" name="button" class="btn-action"data-toggle='modal'
        data-target='#cadclient'><i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar</button>
        <a href={{url('/listclients')}} class="btn-action"><i class="fa fa-eye" aria-hidden="true"></i> Ver todos</a>
      </div>

      <h3 class="text-left">Clientes</h3>

        <div class="table-responsive">
          <table class="table table-hover">
            <thead class="thead-inverse">
              <tr>
                <th class="text-center">Nome</th>
                <th class="text-center">CPF</th>
                <th class="text-center">Tel</th>
                <th class="text-center">Cel</th>
                <th class="text-center">email</th>
                <th class="text-center">cadastro</th>
                <th class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($clients as $client)
                <tr id='2-{{$client->id}}'>
                    <td class="text-center">{{$client->name}}</td>
                    <td class="text-center tdcpf">{{$client->cpf}}</td>
                    <td class="text-center tdtel">{{$client->tel}}</td>
                    <td class="text-center tdcel">{{$client->cel}}</td>
                    <td class="text-center">{{$client->email}}</td>
                    <td class="text-center">{{date_format($client->created_at, 'd/m/Y')}}</td>
                    <td class="text-center">
                      <div class="">

                          <button onclick="checkCols('2-{{$client->id}}','editclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#editmodalclient'>
                          <i class="fa fa-pencil" data-toggle='tooltip'data-placement="top"
                          title="Editar" aria-hidden="true"></i></button>

                          <button onclick="checkCols('2-{{$client->id}}','deleteclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#deletemodalclient'>
                          <i class="fa fa-trash" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Excluir"></i></button>

                          <button onclick="checkCols('2-{{$client->id}}','deleteclient')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#deletemodalclient'>
                          <i class="fa fa-paw" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Pet"></i></button>
                      </div>
                      </td>
                </tr>

              @endforeach
            </tbody>
          </table>
        </div>

    </div>
  </div>



  <script>
  function checkCols (id,tipo)
  {

    var linha = document.getElementById(id);
    var colunas = linha.getElementsByTagName('td');

    var str = id;
    id = str.replace(/2-|1-/,"");

    if(tipo == 'delete'){
    var div = document.getElementById('divdelete');
    div.innerHTML = "<p class='pdelete text-center'> Deseja deletar o usuário "+colunas[0].innerHTML+"</p>";
    document.formdel.action = '{{url('/delete')}}'+'/'+id;
  }else if(tipo == 'deleteclient'){
    var div = document.getElementById('divdeleteclient');
    div.innerHTML = "<p class='pdelete text-center'> Deseja deletar o(a) cliente "+colunas[0].innerHTML+"</p>";
    document.formdelclient.action = '{{url('/deleteclient')}}'+'/'+id;
  }else if(tipo == 'editclient'){
    document.getElementById('editNomeClient').value = colunas[0].innerHTML;
    document.getElementById('editCpfClient').value = colunas[1].innerHTML;
    document.getElementById('editTelClient').value = colunas[2].innerHTML;
    document.getElementById('editCelClient').value = colunas[3].innerHTML;
    document.getElementById('editEmailClient').value = colunas[4].innerHTML;


    document.formeditclient.action = '{{url('/editclient')}}'+'/'+id;
  }else {
      document.getElementById('editNome').value = colunas[0].innerHTML;
      document.getElementById('editEmail').value = colunas[1].innerHTML;
      if(colunas[2].innerHTML == 'Administrador'){
        $('#editRole').append('<option value="1" selected="selected">Administrador</option>');
        $('#editRole').append('<option value="2">Usuário</option>');
      }else {
        $('#editRole').append('<option value="1">Administrador</option>');
        $('#editRole').append('<option value="2" selected="selected">Usuário</option>');
      }
      document.formedit.action = '{{url('/edit')}}'+'/'+id;
    }

  }
  </script>

  <!--Janela modal Cadastrar funcionario-->
  <div class="modal fade" id="cadfun" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Cadastrar Funcionário</span>
          </button>
          <h4 class="modal-title text-center">Cadastrar Funcionário</h4>
        </div>
        <div class="modal-body">

          @if( $errors->register->any())
            <div class="alert alert-danger">
              @foreach ($errors->register->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['url'=>'register'])!!}

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="InputNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="InputNome"
                    placeholder="Nome" focus>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputEmail1">Endereço de e-mail</label>
                    <input name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="InputRole">Função</label>
                      <select class="form-control" name="role_id" id="InputRole">
                        <option value="1">Administrador</option>
                        <option value="2" selected>Usuário</option>
                      </select>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input name="password" type="password" class="form-control" id="InputPassword" placeholder="Password">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Inputpassword_confirm">Confirme o password</label>
                      <input name="password_confirm" type="password" class="form-control" id="Inputpassword_confirm" placeholder="Password">
                    </div>
                </div>
              </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal Cadastrar funcionario-->

  <!--Janela modal Editar funcionario-->
  <div class="modal fade" id="editmodal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Editar Funcionário</span>
          </button>
          <h4 class="modal-title text-center">Editar Funcionário</h4>
        </div>
        <div class="modal-body">

          @if( $errors->edit->any())
            <div class="alert alert-danger pdelete">
              @foreach ($errors->edit->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['method' => 'PATCH','name' =>'formedit'])!!}

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="editNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="editNome"
                    placeholder="Nome" focus>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="editEmail">Endereço de e-mail</label>
                    <input name="email" type="email" class="form-control" id="editEmail" placeholder="Email">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="editRole">Função</label>
                      <select class="form-control" name="role_id" id="editRole">

                      </select>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="editPassword">Novo Password</label>
                    <input name="password" type="password" class="form-control" id="editPassword" placeholder="Password">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="editpassword_confirm">Confirme o password</label>
                      <input name="password_confirm" type="password" class="form-control" id="editpassword_confirm" placeholder="Password">
                    </div>
                </div>
              </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal editar funcionario-->

  <!-- Janela modal Delete Funcionário -->
  <div class="modal fade" tabindex="-1" role="dialog" id="deletemodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body" id="divdelete">
        </div>
        {!!Form::open(['method' => 'DELETE','name' =>'formdel'])!!}
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Deletar</button>
        </div>
        {!!Form::close()!!}
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Janela modal Delete Funcionário -->







  <!--Janela modal Cadastrar Clientes-->
  <div class="modal fade" id="cadclient" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Cadastrar Funcionário</span>
          </button>
          <h4 class="modal-title text-center">Cadastrar Cliente</h4>
        </div>
        <div class="modal-body">

          @if( $errors->registerClient->any())
            <div class="alert alert-danger">
              @foreach ($errors->registerClient->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['url'=>'clientreg'])!!}

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="InputNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="InputNome"
                    placeholder="Nome" focus>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputEmail1">Endereço de e-mail</label>
                    <input name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="InputCpf">C.P.F</label>
                      <input name="cpf" type="text" class="form-control tdcpf" id="InputCpf" placeholder="CPF" onblur="cpfvalidator()">

                      <div class="alert alert-danger alert-dismissible alertcad" style="display: none">
                        <button type="button" class="close" data-dismiss="alert">
                          <span aria-hidden="true">&times</span>
                        </button>
                        CPF inválido!
                      </div>

                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputTel">Telefone</label>
                    <input name="tel" type="text" class="form-control tdtel" id="InputTel" placeholder="Telefone">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="InputCel">Celular</label>
                      <input name="cel" type="text" class="form-control tdcel" id="InputCel" placeholder="Celular">
                    </div>
                </div>
              </div>

            <div class="modal-footer">
              <button id="send" type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal Cadastrar cliente-->

  <!--Janela modal Editar cliente-->
  <div class="modal fade" id="editmodalclient" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Editar Cliente</span>
          </button>
          <h4 class="modal-title text-center">Editar Cliente</h4>
        </div>
        <div class="modal-body">

          @if( $errors->editclient->any())
            <div class="alert alert-danger pdelete">
              @foreach ($errors->editclient->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['method' => 'PATCH','name' =>'formeditclient'])!!}

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="editNomeClient">Nome</label>
                <input name="name" type="text"
                class="form-control" id="editNomeClient"
                placeholder="Nome" focus>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="editEmailClient">Endereço de e-mail</label>
                <input name="email" type="email" class="form-control" id="editEmailClient" placeholder="Email">
              </div>
            </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="editCpfClient">C.P.F</label>
                  <input name="cpf" type="text" class="form-control tdcpf" id="editCpfClient" placeholder="CPF"  onblur="cpfvalidator()">

                  <div class="alert alert-danger alert-dismissible alertedit" style="display: none">
                    <button type="button" class="close" data-dismiss="alert">
                      <span aria-hidden="true">&times</span>
                    </button>
                    CPF inválido!
                  </div>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="editTelClient">Telefone</label>
                <input name="tel" type="text" class="form-control tdtel" id="editTelClient" placeholder="Telefone">
              </div>
            </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="editCelClient">Celular</label>
                  <input name="cel" type="text" class="form-control tdcel" id="editCelClient" placeholder="Celular">
                </div>
            </div>
          </div>

        <div class="modal-footer">
          <button id="send" type="submit" class="btn btn-success">Enviar</button>
          <button class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal editar Cliente-->

  <!-- Janela modal Delete Cliente -->
  <div class="modal fade" tabindex="-1" role="dialog" id="deletemodalclient">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body" id="divdeleteclient">
        </div>
        {!!Form::open(['method' => 'DELETE','name' =>'formdelclient'])!!}
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Deletar</button>
        </div>
        {!!Form::close()!!}
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Janela modal Delete Cliente -->
@endsection
