@extends('layouts.painel')

@section('content')

          @if( isset($errors) && count($errors) > 0 )
            <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                <p>{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['url'=>'edit','method' => 'PATCH'])!!}

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="InputNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="InputNome"
                    placeholder="Nome" value="{{$users->name}}" focus>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputEmail1">Endereço de e-mail</label>
                    <input value="{{$users->email}}" name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="InputRole">Função</label>
                      <select class="form-control" name="role_id" id="InputRole">
                        @if ($users->role_id == 1)
                          <option value="1" selected>Administrador</option>
                          <option value="2">Usuário</option>
                        @else
                          <option value="1">Administrador</option>
                          <option value="2" selected>Usuário</option>
                        @endif
                      </select>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputPassword">Novo password</label>
                    <input name="password" type="password" class="form-control" id="InputPassword" placeholder="Password">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Inputpassword_confirm">Confirme o novo password</label>
                      <input name="password_confirm" type="password" class="form-control" id="Inputpassword_confirm" placeholder="Password">
                    </div>
                </div>
              </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


@endsection
