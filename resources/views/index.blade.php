@extends('layouts.layout')

@section('content')
  <section class="login-form">
    <form >
      <input type="text" class="form-control" placeholder="Usuário">
      <input type="password" class="form-control" placeholder="Senha">
      <button type="submit" name="Logar" class="btn btn-primary btn-block ">Logar</button>
    </form>
  </section>
@endsection
