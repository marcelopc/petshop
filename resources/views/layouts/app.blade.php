<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--CSS do site-->
    <link rel="stylesheet" href="{{url('assets\site\css\style.css')}}">

    <!--Icone do site-->
    <link rel="shortcut icon" href="{{url('assets\site\img\petshop.ico')}}">

    <!-- Última versão CSS compilada e minificada -->
    <link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">

</head>
<body>
  <div class="header">
    <a href="{{url('/')}}"><img class="img-responsive" src="{{url('assets\site\img\head-logo.png')}}" alt="logo"></a>
  </div>

    <div id="app">

          <div class="col-xs-6 col-login">
            @yield('content')
          </div>
          
            <div class="col-xs-6">
                <img class="img-responsive dog" src="{{url('assets\site\img\bordercollie.png')}}" alt="border collie">
            </div>
        </div>




    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
