<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Petshop</title>
    <!-- Última versão CSS compilada e minificada -->
    <link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets\site\css\layoutstyle.css')}}">
    <link rel="stylesheet" href="{{url('assets\site\css\font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('assets\site\css\responsive-calendar.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('assets\site\css\listuser.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('assets\site\css\clientsStyle.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('assets\site\css\dataTables.bootstrap.css')}}" media="screen">
    <link rel="stylesheet" href="{{url('assets\site\css\bootstrap-datepicker.min.css')}}" media="screen">



  </head>
  <body>
      <div class="wrapper">
          <!-- Sidebar Holder -->
          <nav id="sidebar">
              <div class="sidebar-header">
                  <h3>Petshop</h3>
                  <strong><img src="{{url('assets\site\img\dog.ico')}}" alt="dog"></strong>
              </div>

              <ul class="list-unstyled components">
                  <li class="active">
                      <a href={{url('/dashboard')}}>
                          <i class="fa fa-tachometer" aria-hidden="true"></i>
                          Dashboard
                      </a>
                  </li>
                  <li>
                      <a href={{url('/listclients')}}>
                          <i class="fa fa-address-card-o" aria-hidden="true"></i>
                          Clientes
                      </a>
                  </li>
                  @if (Auth::user()->role_id == 1)
                  <li>
                      <a href={{url('/listuser')}}>
                          <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                          Funcionários
                      </a>
                  </li>
                @endif
                  <li>
                      <a href={{url('/vaccines')}}>
                          <i class="fa fa-eyedropper" aria-hidden="true"></i>
                          Vacinas
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <i class="glyphicon glyphicon-send"></i>
                          Contact
                      </a>
                  </li>
              </ul>
          </nav>



          <!-- Page Content Holder -->
          <div id="content">
              <nav class="navbar navbar-default">
                  <div class="container-fluid">

                      <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span class="sr-only">Toggle Sidebar</span>
                        </button>
                      </div>

                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle aSubmenu" data-toggle="dropdown"
                          role="button" aria-haspopup="true"
                          aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                          {{ Auth::user()->name }} </a>
                          <ul class="dropdown-menu">
                            <li><a href="{{ route('logout') }}"
                              onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                              class="aSubmenu"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                              <form id="logout-form"
                              action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}</form>
                          </ul>
                        </li>
                      </ul>

                  </div>
              </nav>
                <div class="content-box">

                  @yield('content')
                </div>
          </div>
      </div>



      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
    <script
      src="http://code.jquery.com/jquery-3.2.1.min.js"
       integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
       crossorigin="anonymous"></script>
    <script src={{url('assets\site\js\jquery.mask.min.js')}}></script>
    <script src={{url('assets\site\js\bootstrap-datepicker.min.js')}}></script>
    <script src={{url('assets\site\locales\bootstrap-datepicker.pt-BR.min.js')}}></script>


    <!-- Última versão JavaScript compilada e minificada -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
    crossorigin="anonymous"></script>
    <script src={{url('assets\site\js\responsive-calendar.min.js')}}></script>
    <script src={{url('assets\site\js\jquery.dataTables.min.js')}}></script>
    <script src={{url('assets\site\js\dataTables.bootstrap.min.js')}}></script>
    <script src={{url('assets\site\js\main.js')}}></script>



    @if ($errors->register->any())
      <script>
        $(document).ready(function(){
          $('#cadfun').modal('show');
        });
      </script>
    @elseif ($errors->edit->any())
      <script>
        $(document).ready(function(){
          $('#editmodal').modal('show');
        });
      </script>
    @elseif ($errors->registerClient->any())
      <script>
        $(document).ready(function(){
          $('#cadclient').modal('show');
        });
      </script>
    @elseif ($errors->editclient->any())
      <script>
        $(document).ready(function(){
          $('#editmodalclient').modal('show');
        });
      </script>
    @endif
  </body>
</html>
