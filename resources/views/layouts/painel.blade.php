<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!--Icone do site-->
    <link rel="shortcut icon" href="{{url('assets\site\img\petshop.ico')}}">

    <!-- Última versão CSS compilada e minificada -->
    <link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
    <link href="{{url('assets\site\css\font-awesome.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets\site\css\navbar-fixed-side.css')}}" rel="stylesheet" />
    <link href="{{url('assets\site\css\clientsStyle.css')}}" rel="stylesheet" />



</head>
  <body>

    <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-lg-2">
        <nav class="navbar navbar-fixed-side navbar-inverse">

          <div class="container">
            <div class="navbar-header">
              <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <h3 class="text-centermenu">Menu</h3>
            </div>

            <!--Accordion -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              @if (Auth::user()->role_id == '1')
                <div class="panel panel-default ">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a onclick="mudaSeta(1)" class="menu" role="button" data-toggle="collapse"
                      data-parent="#accordion" href="#collapseOne"
                      aria-expanded="true" aria-controls="collapseOne">
                        Admin <span id="seta1" class="glyphicon glyphicon-menu-down"></span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <ul>
                        <li><button type="button" class="btn-modal"
                          data-toggle='modal' data-target='#cadfun'
                          >Adicionar funcionário</button></li>
                        <li><a href="{{url('/painel')}}"><div class="div-modal">Listar funcionários
                        </div></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              @endif

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                  <h4 class="panel-title">
                    <a onclick="mudaSeta(2)" class="collapsed menu" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Funcionário <span  id="seta2" class="glyphicon glyphicon-menu-down"></span>
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">
                    <ul>
                      <li><button type="button" class="btn-modal">Cadastrar Cliente</button></li>
                      <li>Deletar Cliente.</li>
                      <li>Alterar Cliente.</li>
                      <li><a href="{{url('/clients')}}"><div class="div-modal">Listar funcionários
                      </div></a></li>
                      <li>Cadastrar Pet.</li>
                    </ul>
                  </div>
                </div>
              </div>

              <!-- Logout-->
              <div class="panel panel-default">
                 <div class="panel-heading" role="tab" id="headingThree">
                   <h4 class="panel-title">
                     <a onclick="mudaSeta(3)" class="collapsed menu" role="button"
                     data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                     aria-expanded="false" aria-controls="collapseThree">
                     {{ Auth::user()->name }}
                      <span id="seta3" class="glyphicon glyphicon-menu-down"></span>
                     </a>
                   </h4>
                 </div>
                 <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                   <div class="panel-body">

                       <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                                    class="logout">
                           <i class="fa fa-sign-out" aria-hidden="true"> Logout</i>
                       </a>



                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         {{ csrf_field() }}
                     </form>
                   </div>
                 </div>
               </div>

              <!--./Logout-->
            </div>
            <!-- ./Accordion-->


          </div>
        </nav>
      </div>
        <div class="col-sm-9 col-lg-10 pad">

          <div class="container">

          @yield('content')
          </div>


        </div>
      </div>
    </div>
  </div>

  <!--Janela modal Cadastrar funcionario-->
  <div class="modal fade" id="cadfun" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Cadastrar Funcionário</span>
          </button>
          <h4 class="modal-title text-center">Cadastrar Funcionário</h4>
        </div>
        <div class="modal-body">

          @if( $errors->register->any())
            <div class="alert alert-danger">
              @foreach ($errors->register->all() as $error)
                <p>{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['url'=>'register'])!!}

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label for="InputNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="InputNome"
                    placeholder="Nome" focus>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputEmail1">Endereço de e-mail</label>
                    <input name="email" type="email" class="form-control" id="InputEmail1" placeholder="Email">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="InputRole">Função</label>
                      <select class="form-control" name="role_id" id="InputRole">
                        <option value="1">Administrador</option>
                        <option value="2" selected>Usuário</option>
                      </select>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input name="password" type="password" class="form-control" id="InputPassword" placeholder="Password">
                  </div>
                </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Inputpassword_confirm">Confirme o password</label>
                      <input name="password_confirm" type="password" class="form-control" id="Inputpassword_confirm" placeholder="Password">
                    </div>
                </div>
              </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal Cadastrar funcionario-->
  <script src={{url('assets\site\js\main.js')}}></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    @if ($errors->register->any())
      <script>
        $(document).ready(function(){
          $('#cadfun').modal('show');
        });
      </script>
    @elseif ($errors->edit->any())
      <script>
        $(document).ready(function(){
          $('#editmodal').modal('show');
        });
      </script>
    @endif


  </body>
</html>
