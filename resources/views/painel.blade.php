@extends('layouts.painel')

@section('content')
  @can('admin',Auth::user()->role_id)
    <h3 class="text-center">Lista de Funcionários</h3>
    <table class="table table-striped table-hover">
      <thead class="thead-inverse">
        <tr>
          <th class="text-center">Nome</th>
          <th class="text-center">E-mail</th>
          <th class="text-center">Função</th>
          <th class="text-center">Cadastro</th>
          <th class="text-center">Ações</th>
        </tr>
      </thead>
      <tbody>
       @foreach ($users as $user)

          <tr id={{$user->id}}>
              <td class="text-center">{{$user->name}}</td>
              <td class="text-center">{{$user->email}}</td>
              <td class="text-center">{{$user->role}}</td>
              <td class="text-center">{{date_format($user->created_at, 'd/m/Y')}}</td>
              <td class="text-center">
                <div class="">

                    <button onclick="checkCols({{$user->id}},'edit')" type="button" name="button" class="btn-action"
                    data-toggle='modal' data-target='#editmodal'><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button onclick="checkCols({{$user->id}},'delete')" type="button" name="button" class="btn-action"
                    data-toggle='modal' data-target='#deletemodal'><i class="fa fa-trash" aria-hidden="true"></i></button>
                </div>
                </td>
          </tr>

        @endforeach
      </tbody>
    </table>


    <script>
    function checkCols (id,tipo)
    {
      var linha = document.getElementById(id);
      var colunas = linha.getElementsByTagName('td');
      var div = document.getElementById('divdelete');
      if(tipo == 'delete'){
      div.innerHTML = "<p> Deseja deletar o usuário "+colunas[0].innerHTML+"</p>";
      document.formdel.action = '{{url('/delete')}}'+'/'+id;
      }else {
        document.getElementById('editNome').value = colunas[0].innerHTML;
        document.getElementById('editEmail').value = colunas[1].innerHTML;
        if(colunas[2].innerHTML == 'Administrador'){
          $('#editRole').append('<option value="1" selected="selected">Administrador</option>');
          $('#editRole').append('<option value="2">Usuário</option>');
        }else {
          $('#editRole').append('<option value="1">Administrador</option>');
          $('#editRole').append('<option value="2" selected="selected">Usuário</option>');
        }
        document.formedit.action = '{{url('/edit')}}'+'/'+id;
      }

    }
    </script>



    <!--Janela modal Cadastrar funcionario-->
    <div class="modal fade" id="editmodal" role="dialog" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">&times</span>
              <span class="sr-only">Editar Funcionário</span>
            </button>
            <h4 class="modal-title text-center">Editar Funcionário</h4>
          </div>
          <div class="modal-body">

            @if( $errors->edit->any())
              <div class="alert alert-danger">
                @foreach ($errors->edit->all() as $error)
                  <p>{{$error}}</p>
                @endforeach
              </div>
            @endif
            {!!Form::open(['method' => 'PATCH','name' =>'formedit'])!!}

                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="editNome">Nome</label>
                      <input name="name" type="text"
                      class="form-control" id="editNome"
                      placeholder="Nome" focus>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="editEmail">Endereço de e-mail</label>
                      <input name="email" type="email" class="form-control" id="editEmail" placeholder="Email">
                    </div>
                  </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="editRole">Função</label>
                        <select class="form-control" name="role_id" id="editRole">

                        </select>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="editPassword">Novo Password</label>
                      <input name="password" type="password" class="form-control" id="editPassword" placeholder="Password">
                    </div>
                  </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="editpassword_confirm">Confirme o password</label>
                        <input name="password_confirm" type="password" class="form-control" id="editpassword_confirm" placeholder="Password">
                      </div>
                  </div>
                </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-success">Enviar</button>
                <button class="btn btn-default" data-dismiss="modal">Fechar</button>
              </div>
              {!!Form::close()!!}


          </div>

        </div>
      </div>
    </div>
    <!--Janela modal Cadastrar funcionario-->




    <div class="modal fade" tabindex="-1" role="dialog" id="deletemodal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete</h4>
          </div>
          <div class="modal-body" id="divdelete">
          </div>
          {!!Form::open(['method' => 'DELETE','name' =>'formdel'])!!}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Deletar</button>
          </div>
          {!!Form::close()!!}
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  @endcan()
@endsection
