@extends('layouts.layout')

@section('content')


  <div class="col-lg-12">
    <div class="contentlistclients">
      <div class="btn-right">
        <button type="button" name="button" class="btn-action"data-toggle='modal'
        data-target='#cadVaccine'><i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar</button>
      </div>

      <h3 class="text-center">Vacinas</h3>


          <table  class="table table-striped table-bordered table-hover dataTableVaccine no-footer" cellspacing="0">
            <thead class="thead-inverse">
              <tr>
                <th class="text-center">Nome</th>
                <th class="text-center">validade</th>
                <th class="text-center">Ações</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($vaccines as $vaccine)
                <tr id='{{$vaccine->id}}'>
                  <td class="text-center">{{$vaccine->name}}</td>
                  @if ($vaccine->shelf_life == 30)
                    <td class="text-center">Mensal</td>
                  @elseif ($vaccine->shelf_life == 60)
                    <td class="text-center">Bimestral</td>
                  @elseif ($vaccine->shelf_life == 90)
                    <td class="text-center">Trimestral</td>
                  @elseif ($vaccine->shelf_life == 120)
                    <td class="text-center">Quadrimestral</td>
                  @elseif ($vaccine->shelf_life == 150)
                    <td class="text-center">Quimestral</td>
                  @elseif ($vaccine->shelf_life == 180)
                    <td class="text-center">Semestral</td>
                  @elseif ($vaccine->shelf_life == 360)
                    <td class="text-center">Anual</td>

                  @endif
                    <td class="text-center">
                      <div>

                          <button onclick="checkCols('{{$vaccine->id}}','editvaccine')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#editVaccine'>
                          <i class="fa fa-pencil" data-toggle='tooltip'data-placement="top"
                          title="Editar" aria-hidden="true"></i></button>

                          <button onclick="checkCols('{{$vaccine->id}}','deletevaccine')" type="button" name="button" class="btn-action"
                          data-toggle='modal' data-target='#deletevaccine'>
                          <i class="fa fa-trash" aria-hidden="true" data-toggle='tooltip'data-placement="top"
                          title="Excluir"></i></button>

                      </div>
                      </td>
                </tr>

              @endforeach
            </tbody>

          </table>


    </div>
  </div>


  <script>
  function checkCols (id,tipo)
  {
    var linha = document.getElementById(id);
    var colunas = linha.getElementsByTagName('td');


    if(tipo == 'deletevaccine'){
    var div = document.getElementById('divdeletevaccine');
    div.innerHTML = "<p class='pdelete text-center'> Deseja deletar a vacina "+colunas[0].innerHTML+"</p>";
    document.formdelvaccine.action = '{{url('/deletevaccine')}}'+'/'+id;
  }else {
      document.getElementById('EditNome').value = colunas[0].innerHTML;
      if(colunas[1].innerHTML == 'Mensal'){
        $("option").remove()
        $('#EditShelf_life').append('<option value="30" selected="selected">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else if(colunas[1].innerHTML == 'Bimestral') {
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60" selected="selected">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else if(colunas[1].innerHTML == 'Trimestral') {
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90" selected="selected">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else if(colunas[1].innerHTML == 'Quadrimestral') {
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120" selected="selected">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else if(colunas[1].innerHTML == 'Quimestral') {
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150" selected="selected">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else if(colunas[1].innerHTML == 'Semestral') {
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180" selected="selected">Semestral</option>');
        $('#EditShelf_life').append('<option value="360">Anual</option>');
      }else{
        $("option").remove()
        $('#EditShelf_life').append('<option value="30">Mensal</option>');
        $('#EditShelf_life').append('<option value="60">Bimestral</option>');
        $('#EditShelf_life').append('<option value="90">Trimestral</option>');
        $('#EditShelf_life').append('<option value="120">Quadrimestral</option>');
        $('#EditShelf_life').append('<option value="150">Quimestral</option>');
        $('#EditShelf_life').append('<option value="180">Semestral</option>');
        $('#EditShelf_life').append('<option value="360" selected="selected">Anual</option>');
      }
      document.formeditvaccine.action = '{{url('/editvaccine')}}'+'/'+id;
    }

  }
  </script>

  <!--Janela modal Cadastrar Vacina-->
  <div class="modal fade" id="cadVaccine" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Cadastrar Vacina</span>
          </button>
          <h4 class="modal-title text-center">Cadastrar Vacina</h4>
        </div>
        <div class="modal-body">

          @if( $errors->register->any())
            <div class="alert alert-danger">
              @foreach ($errors->register->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif

            {!!Form::open(['url'=>'registervaccine'])!!}

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="InputNome">Nome</label>
                    <input name="name" type="text"
                    class="form-control" id="InputNome"
                    placeholder="Nome" focus>
                  </div>
                </div>
                <div class="col-lg-6">
                    <label for="InputShelf_life">Validade</label>
                    <select class="form-control" name="shelf_life" id="InputShelf_life">
                      <option value="30">Mensal</option>
                      <option value="60">Bimestral</option>
                      <option value="90">Trimestral</option>
                      <option value="120">Quadrimestral</option>
                      <option value="150">Quimestral</option>
                      <option value="180">Semestral</option>
                      <option value="360">Anual</option>
                    </select>
                </div>
              </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Enviar</button>
              <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal Cadastrar Vacina-->

  <!--Janela modal Editar cliente-->
  <div class="modal fade" id="editVaccine" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times</span>
            <span class="sr-only">Editar Vacina</span>
          </button>
          <h4 class="modal-title text-center">Editar Vacina</h4>
        </div>
        <div class="modal-body">

          @if( $errors->editclient->any())
            <div class="alert alert-danger pdelete">
              @foreach ($errors->editclient->all() as $error)
                <p class="pdelete">{{$error}}</p>
              @endforeach
            </div>
          @endif
          {!!Form::open(['method' => 'PATCH','name' =>'formeditvaccine'])!!}

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="EditNome">Nome</label>
                <input name="name" type="text"
                class="form-control" id="EditNome"
                placeholder="Nome" focus>
              </div>
            </div>
            <div class="col-lg-6">
                <label for="EditShelf_life">Validade</label>
                <select class="form-control" name="shelf_life" id="EditShelf_life">
                </select>
            </div>
          </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Enviar</button>
          <button class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
            {!!Form::close()!!}


        </div>

      </div>
    </div>
  </div>
  <!--Janela modal editar vacina-->

  <!-- Janela modal Delete vacina -->
  <div class="modal fade" tabindex="-1" role="dialog" id="deletevaccine">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Deletar Vacina</h4>
        </div>
        <div class="modal-body" id="divdeletevaccine">
        </div>
        {!!Form::open(['method' => 'DELETE','name' =>'formdelvaccine'])!!}
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Deletar</button>
        </div>
        {!!Form::close()!!}
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Janela modal Delete vacina -->
@endsection
