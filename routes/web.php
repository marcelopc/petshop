<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

route::group(['middleware' => 'web'], function () {
  Route::post('/register', 'UserController@register');
  Route::post('/clientreg', 'ClientController@register');
  Route::post('/registervaccine', 'VaccinesController@register');

  Route::get('/dashboard', 'UserController@index');
  Route::get('/listuser', 'UserController@list');
  Route::get('/listclients', 'ClientController@list');
  Route::get('/vaccines', 'VaccinesController@list');

  Route::patch('/edit/{user}', 'UserController@update');
  Route::patch('/editclient/{user}', 'ClientController@update');
  Route::patch('/editvaccine/{user}', 'VaccinesController@update');

  Route::delete('delete/{user}', 'UserController@delete');
  Route::delete('deleteclient/{user}', 'ClientController@delete');
  Route::delete('deletevaccine/{user}', 'VaccinesController@delete');
});

Route::get('/', 'HomeController@index')->name('home');
